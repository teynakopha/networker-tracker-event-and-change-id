﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Diagnostics.Eventing.Reader;
using System.Xml.Linq;
using System.IO;

namespace NetwokerService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            createFileConfig();
            eventLogSubscription();
        }

        protected override void OnStop()
        {

        }
        public static void createFileConfig()
        {
            if (!File.Exists("config.xml")) //ถ้าไม่มี file
            {
                XDocument srcTree = new XDocument
                (
                    new XComment("This is a comment"),
                    new XElement("NetworkerConfig",
                        new XElement("EventID", "1"),
                        new XElement("ProviderName", "Networker"),
                        new XElement("word",
                            new XElement("level", "1"),
                            new XElement("keyword", "tesyt1")),
                        new XElement("word",
                            new XElement("level", "2"),
                            new XElement("keyword", "tesyt3"))
                        )
                 );
                srcTree.Save("config.xml");
                //Debug test
                EventLog eventLog = new EventLog("Application");
                String path = "C:\\temp";
                eventLog.Source = "Networker01";
                eventLog.WriteEvent(new EventInstance(1999, 0, EventLogEntryType.Information), new object[] { "file config not found  ", path });
                //End Debug 
            }
            else
            {
                //Debug test
                EventLog eventLog = new EventLog("Application");
                String path = "C:\\temp";
                eventLog.Source = "Networker01";
                eventLog.WriteEvent(new EventInstance(1999, 0, EventLogEntryType.Information), new object[] { "file config.xml have already ", path });
                //End Debug 
            }
        }

        public static void eventLogSubscription()
        {
            using (EventLog eventLog = new EventLog("Application"))
            {
                String path = "C:\\temp";
                eventLog.Source = "Networker01";
                eventLog.WriteEvent(new EventInstance(1999, 0, EventLogEntryType.Information), new object[] { "The event log watcher has started", path });
                //string a = System.Environment.CurrentDirectory;
                //eventLog.WriteEvent(new EventInstance(1999, 0, EventLogEntryType.Information), new object[] { a, path });
                eventLog.Dispose();
            }
            EventLogWatcher watcher = null;

            try
            {
                createFileConfig();

                XDocument data = XDocument.Load("C:\\Users\\Administrator\\Desktop\\tracker\\config.xml");
                var selectProviderName = from providerName in data.Descendants("NetworkerConfig") select providerName;
                var selectConfig = from wordFilter in data.Descendants("word")
                                   select wordFilter;                
                XElement AppConf = selectProviderName.ElementAt(0);

                string eventQueryString = String.Format("*[System/Provider/@Name =\"{0}\"]", AppConf.Element("ProviderName").Value);//"*[System/EventID=19]";// + " and [Application/@Provider Name=]";
                EventLogQuery eventQuery = new EventLogQuery(
                    "Application", PathType.LogName, eventQueryString);
                watcher = new EventLogWatcher(eventQuery);
                watcher.EventRecordWritten += new EventHandler<EventRecordWrittenEventArgs>(handlerExplorerLaunch);
                watcher.Enabled = true;
            }
            catch (EventLogReadingException e)
            {
                //Console.WriteLine("Error reading the log: {0}", e.Message);
            }
        }

        public static void handlerExplorerLaunch(object obj, EventRecordWrittenEventArgs arg)
        {
            if (arg.EventRecord != null)
            {
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "NetWorker01";
                    //Console.WriteLine("Event Level" + arg.EventRecord.Level + " event id: " + arg.EventRecord.Id + " event timecreate: " + arg.EventRecord.TimeCreated +
                    // " Keyword " + arg.EventRecord.Keywords + " Bookmark " + arg.EventRecord.Bookmark + " Task: " + arg.EventRecord.Task + " Task display:"
                    //+ arg.EventRecord.TaskDisplayName + " opcode:" + arg.EventRecord.Opcode);
                    // Console.WriteLine("description : " + arg.EventRecord.FormatDescription());
                    //read word filtering ว่าตรง ใน word ใหม ถ้าตรง เขียน log
                    XDocument data = XDocument.Load("C:\\Users\\Administrator\\Desktop\\tracker\\config.xml");
                    var selectConfig = from wordFilter in data.Descendants("word")
                                       select wordFilter;
                    int i = 0;
                    // check container word 
                    foreach (XElement word in selectConfig)
                    {
                        string keyword = word.Element("keyword").Value;
                        string eventMessage = arg.EventRecord.FormatDescription();
                        int level = Int32.Parse(word.Element("level").Value);
                        string ms = String.Format("find keyword: {0} level: {1}",keyword,level);
                        eventLog.WriteEvent(new EventInstance(1999, 0, EventLogEntryType.Information), new object[] {ms});
                        if (eventMessage.Contains(keyword))
                        {
                            int id = Int32.Parse(word.Element("EventID").Value);
                            var eventType = EventLogEntryType.Information;
                            i = i + 1;
                            switch (level)
                            {
                                case 1:
                                    eventType = EventLogEntryType.Error;
                                    break;
                                case 2:
                                    eventType = EventLogEntryType.FailureAudit;
                                    break;
                                case 4:
                                    eventType = EventLogEntryType.SuccessAudit;
                                    break;
                                case 5:
                                    eventType = EventLogEntryType.Warning;
                                    break;
                                default:
                                    eventType = EventLogEntryType.Information;
                                    break;
                            }
                            eventLog.WriteEvent(new EventInstance(id, 0, eventType), new object[] { eventMessage });
                        }
                    }
                    if (i <= 0)
                    {
                        EventLogEntryType eventType = EventLogEntryType.Information;
                        int level = Int32.Parse(arg.EventRecord.Level.ToString());

                        switch (level)
                        {
                            case 1:
                                eventType = EventLogEntryType.Error;
                                break;
                            case 2:
                                eventType = EventLogEntryType.FailureAudit;
                                break;
                            case 4:
                                eventType = EventLogEntryType.SuccessAudit;
                                break;
                            case 5:
                                eventType = EventLogEntryType.Warning;
                                break;
                            default:
                                eventType = EventLogEntryType.Information;
                                break;
                        }
                        eventLog.WriteEvent(new EventInstance(2000, 0, eventType), new object[] { arg.EventRecord.FormatDescription() });
                    }
                    //eventLog.WriteEntry(arg.EventRecord.ToXml(), EventLogEntryType.Information, 1001, 1);
                    eventLog.Dispose();
                }
            }
            else
            {
                //  .WriteLine("The event instance was null.");
            }
        }
    }
}
